A custom dashboard display for a first generation Kawasaki ER6 motorcycle (A.K.A. ER650/Ninja650 etc.). Non-intrusive installation - communicates by a diagnostics port on the motorcycle. Possible future expansion for other Kawasaki models.

First revision plans:

  - a L9637D driver for a K-Line communication with an ECU,
  - a small SMPS module providing 5V for the whole,
  - a STM32F103 main board (Blue Pill) with an Arduino bootloader,
  - a seven-segment display for gear indication,
  - a 10 element bar display for fuel level, temperatures or RPM values,
  - a button or two,
  - a custom 3d printed enclosure.

Keywords: kawasaki, dashboard, ecu, kds, arduino, display, stm32, k-line, kwp2000